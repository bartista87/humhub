\|
`Contributing <CONTRIBUTING.rst>`__  \|
`Help & Support <https://www.sbublies.de>`__ \|
`Settings <Documentation/Settings.cfg>`__ \|

Extension Manual from EXT:humhub
=========================================

:Repository:  https://gitlab.com/bartista87/humhub/
:Read online: https://docs.typo3.org/p/sbublies/humhub/main/en-us/


About this Extension
---------------------

This extension brings a FE user syncronization (add, update, delete, hidden) as well as two SSO plugins directly to HumHub


Found a problem in this repo?
-----------------------------

If you find any problems in this extension manual, please add an `Issue`_,
or contact the documentation team via Slack or Email (see `Help & Support`_).

.. _Help & Support: https://sbublies.de
.. _Issue: https://gitlab.com/bartista87/humhub/-/issues
