# customcategory=basic=HumHub installation informations

# cat=basic/enable; type=string; label= HumHub base url (e.g. yourhost.com / identifier.humhub.com)
baseurl =

# cat=basic/enable; type=string; label= HumHub username
username =

# cat=basic/enable; type=string; label= HumHub password
password =

# cat=basic/enable; type=string; label= HumHub JWT sharedKey, please use a 256bit strong key
sharedKey =

# cat=basic/enable; type=string; label= Valid time in seconds
keyValid = 60

# cat=basic/enable; type=string; label= Sync period in hours (e.g. -24 means: the last sync from the fe user is older than 24 hours)
syncPeriod = -24

# cat=basic/enable; type=boolean; label= HumHub installation with enabled Pretty URLs
prettyIUrls = 1

# cat=basic/enable; type=string; label= HumHub api version
api = v1
