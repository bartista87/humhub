<?php
namespace SBublies\Humhub\Tasks;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;

class SyncronizationTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{
    public function execute(){
        // field values
        $feStorageId = $this->feStorageId;
        $adminMail = ($this->humhubmail ?? false);
        $syncId = $this->syncfielduid;

        $syncronization = new \SBublies\Humhub\Services\HumHubSyncronization();
        $syncronization->syncData($feStorageId,$syncId);
        $result = true;

        // Send Mail to Admin
        if ($syncronization)
        {
            try {

                if($adminMail) {
                    $this->sendSuccessfullMailToAdmin($adminMail);
                }
                $result = true;

            } catch(\Exception $e)
            {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * @param $mail
     * @return bool
     * (c) 2023 by Stefan Bublies@dt-internet.de
     */

    protected function sendSuccessfullMailToAdmin($mail)
    {
        $bodyText = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mail.body', 'humhub');
        $mail = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class)
            ->from(new \Symfony\Component\Mime\Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress']))
            ->to(
                new \Symfony\Component\Mime\Address($mail)
            )
            ->setSubject(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mail.subject', 'humhub'))
            ->text($bodyText)
            ->send();
        return true;
    }

}
