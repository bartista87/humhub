<?php

declare(strict_types=1);

namespace SBublies\Humhub\Controller;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use Firebase\JWT\JWT;
use SBublies\Humhub\Utility\ExtConfigurationUtility;
use SBublies\Humhub\Utility\FeUserUtility;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility as Locallang;

/**
 * HumHubLoginController
 */
class HumHubLoginController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action index
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function indexAction(): \Psr\Http\Message\ResponseInterface
    {

        // read fe user
        $feUserData = FeUserUtility::getCurrentUser();

        $this->view->assignMultiple([
            'humhuburl' => $this->generateToken($feUserData),
        ]);

        return $this->htmlResponse();
    }


    /**
     * action formview
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function formviewAction(): \Psr\Http\Message\ResponseInterface
    {
        // get form arguments
        $arguments = $this->request->getArguments();

        // if form is submitted
        if(isset($arguments['humhublogin'])) {
            // fe-user storage from plugin
            $pidList = $this->configurationManager->getConfiguration(
                \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
            )['persistence']['storagePid'];

            // read fe user
            $readFeUser = FeUserUtility::readFeUser($pidList, trim($arguments['humhublogin']['username']));

            if ($readFeUser) {
                // validate user
                $validateUser = GeneralUtility::makeInstance(PasswordHashFactory::class)
                    ->get($readFeUser[0]['password'], 'FE')
                    ->checkPassword($arguments['humhublogin']['password'], $readFeUser[0]['password']);

                // get token with redirect url and redirect to humhub
                if ($validateUser) {
                    $uri = $this->generateToken($readFeUser[0]);
                    return new RedirectResponse($uri, 200);
                } else {
                    $this->addFlashMessage(
                        Locallang::translate('formAuth.error.body', 'humhub', null),
                        Locallang::translate('formAuth.error.header', 'humhub', null),
                        \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::ERROR
                    );
                }
            } else {
                $this->addFlashMessage(
                    Locallang::translate('formAuth.feuser.error.body', 'humhub', null),
                    Locallang::translate('formAuth.feuser.error.header', 'humhub', null),
                    \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::ERROR
                );
            }

        }

        return $this->htmlResponse();
    }

    /**
     * generateToken
     * @param $feuser
     * @return string
     */
    private function generateToken($feuser): string
    {
        // Configuration
        $sharedKey = ExtConfigurationUtility::getConfig()['sharedKey'];
        $humhubDomain = 'https://'.ExtConfigurationUtility::getConfig()['baseurl'];
        $urlRewritingEnabled = ExtConfigurationUtility::getConfig()['prettyIUrls'];

        // Build token including your user data
        $now = time();
        $payload = array(
            'iss' => 'tx_humhub',
            'jti' => md5($now . rand()),
            'guid' => $feuser['uid'],
            'iat' => $now,
            'exp' => $now + ExtConfigurationUtility::getConfig()['keyValid'],
            'username' => $feuser['username'],
            'email' => $feuser['email']
        );

        // Create JWT token
        $JwtToken = JWT::encode($payload, $sharedKey, 'HS256');

        // Redirect user back to humhub
        if ($urlRewritingEnabled) {
            $uri = $humhubDomain . '/user/auth/external?authclient=jwt';
        } else {
            $uri = $humhubDomain . '/index.php?r=/user/auth/external&authclient=jwt';
        }
        $uri .= "&jwt=" . $JwtToken;

        return $uri;
    }
}
