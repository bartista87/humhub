<?php
namespace SBublies\Humhub\Domain\Model;


/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/


/**
 * Mapping
 */
class Mapping extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * typo3 field
     *
     * @var string
     */
    protected $t3field = '';

    /**
     * humhub field
     *
     * @var string
     */
    protected $humhubfield = '';

    /**
     * humhub array field
     *
     * @var string
     */
    protected $humhubarray = '';


    /**
     * Returns the t3field
     *
     * @return string $t3field
     */
    public function getT3field()
    {
        return $this->t3field;
    }

    /**
     * Sets the t3field
     *
     * @param string $t3field
     * @return void
     */
    public function setT3field($t3field)
    {
        $this->t3field = $t3field;
    }

    /**
     * Returns the humhubfield
     *
     * @return string $humhubfield
     */
    public function getHumhubfield()
    {
        return $this->humhubfield;
    }

    /**
     * Sets the humhubfield
     *
     * @param string $humhubfield
     * @return void
     */
    public function setHumhubfield($humhubfield)
    {
        $this->humhubfield = $humhubfield;
    }

    /**
     * Returns the humhubarray
     *
     * @return string $humhubarray
     */
    public function getHumhubarray()
    {
        return $this->humhubarray;
    }

    /**
     * Sets the humhubarray
     *
     * @param string $humhubarray
     * @return void
     */
    public function setHumhubarray($humhubarray)
    {
        $this->humhubarray = $humhubarray;
    }
}
