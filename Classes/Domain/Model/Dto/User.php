<?php
declare(strict_types=1);
namespace SBublies\Humhub\Domain\Model\Dto;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

/**
 * Class User
 */

class User extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * lastsynctohumhub
     *
     * @var int
     */
    protected $lastsynctohumhub = '';

    /**
     * humhubsyncstate
     *
     * @var string
     */
    protected $humhubsyncstate = '';

    /**
     * humhuberror
     *
     * @var string
     */
    protected $humhuberror = '';

    /**
     * @return string
     */
    public function getLastsynctohumhub(): string
    {
        return $this->lastsynctohumhub;
    }

    /**
     * @param string $lastsynctohumhub
     */
    public function setLastsynctohumhub(string $lastsynctohumhub)
    {
        $this->lastsynctohumhub = $lastsynctohumhub;
    }

    /**
     * @return string
     */
    public function getHumhubsyncstate(): string
    {
        return $this->humhubsyncstate;
    }

    /**
     * @param string $humhubsyncstate
     */
    public function setHumhubsyncstate(string $humhubsyncstate)
    {
        $this->humhubsyncstate = $humhubsyncstate;
    }

    /**
     * @return string
     */
    public function getHumhuberror(): string
    {
        return $this->humhuberror;
    }

    /**
     * @param string $humhuberror
     */
    public function setHumhuberror(string $humhuberror)
    {
        $this->humhuberror = $humhuberror;
    }

}
