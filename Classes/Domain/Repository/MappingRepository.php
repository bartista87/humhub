<?php
namespace SBublies\Humhub\Domain\Repository;


/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;


/**
 * The repository for Mappings
 */
class MappingRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @param $pid
     * @return array
     */
    public function findByPid($pid): array
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_humhub_domain_model_mapping');
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from('tx_humhub_domain_model_mapping')
            ->where(
                $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($pid))
            )
            ->executeQuery()
            ->fetchAll(\Doctrine\DBAL\FetchMode::ASSOCIATIVE);

        return $results;
    }


    /**
     * @var array
     */
    protected $defaultOrderings = ['uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];
}
