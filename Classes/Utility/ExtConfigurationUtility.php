<?php
namespace SBublies\Humhub\Utility;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

class ExtConfigurationUtility
{

    /**
     * @return array
     */
    public static function getConfig(): array
    {
        $extConfig = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['humhub'];
        return $extConfig;
    }
}
