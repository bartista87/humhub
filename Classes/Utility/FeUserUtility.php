<?php
namespace SBublies\Humhub\Utility;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;

class FeUserUtility
{

    /**
     * @param $feUserStorage
     * @return mixed[]|\mixed[][]
     * @throws \Doctrine\DBAL\Exception
     */
    public static function readFeUserData($feUserStorage) {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('fe_users');
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from('fe_users')
            ->where(
                $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($feUserStorage))
            )
            ->executeQuery()
            ->fetchAll(\Doctrine\DBAL\FetchMode::ASSOCIATIVE);

        return $results;

    }

    /**
     * @param $feUserStorage
     * @return mixed[]|\mixed[][]
     * @throws \Doctrine\DBAL\Exception
     */
    public static function readAllFeUserData($feUserStorage) {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('fe_users');
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll();

        $results = $queryBuilder
            ->select('*')
            ->from('fe_users')
            ->where(
                $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($feUserStorage))
            )
            ->executeQuery()
            ->fetchAll(\Doctrine\DBAL\FetchMode::ASSOCIATIVE);

        return $results;

    }

    /**
     * @param $feUserStorage
     * @param $username
     * @return mixed[]|\mixed[][]
     * @throws \Doctrine\DBAL\Exception
     */
    public static function readFeUser($feUserStorage, $username) {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('fe_users');
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from('fe_users')
            ->where(
                $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($feUserStorage)),
                $queryBuilder->expr()->eq('username', $queryBuilder->createNamedParameter($username))
            )
            ->executeQuery()
            ->fetchAll(\Doctrine\DBAL\FetchMode::ASSOCIATIVE);

        return $results;

    }

    /**
     * @return array|void
     */
    public static function getCurrentUser()
    {
        if ($GLOBALS['TSFE']->fe_user) {
            return $GLOBALS['TSFE']->fe_user->user ?? [];
        }
    }

    /**
     * @param $userId
     * @param $state
     * @param $errorResponse
     * @return void
     */
    public static function updateFeUser($userId, $state, $errorResponse): void
    {
        if ($state == 400) {
            $stateResult = 'ERROR';
            $syncTime = 0;
        } else {
            $stateResult = 'COMPLETE';
            $syncTime = time();
        }

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('fe_users');
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->update('fe_users')
            ->set('lastsynctohumhub', $syncTime)
            ->set('humhubsyncstate', $stateResult)
            ->set('humhuberror', $errorResponse)
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($userId))
            )
        ;
        $query->executeStatement();
    }
}
