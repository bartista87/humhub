<?php
namespace SBublies\Humhub\Utility;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use SBublies\Humhub\Utility\ExtConfigurationUtility;

class HumHubAuth
{
    /**
     * @return mixed
     * (c) 2023 Stefan Bublies
     */
    public static function getJwTAuth()
    {
        $jsonData = array(
            'username'=> ExtConfigurationUtility::getConfig()['username'],
            'password' => ExtConfigurationUtility::getConfig()['password']
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_URL, "https://".ExtConfigurationUtility::getConfig()['baseurl']."/api/v1/auth/login" );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST, 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}
