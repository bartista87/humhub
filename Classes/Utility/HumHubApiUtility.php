<?php
namespace SBublies\Humhub\Utility;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use SBublies\Humhub\Utility\ExtConfigurationUtility;
use SBublies\Humhub\Utility\HumHubAuth;

class HumHubApiUtility
{

    /**
     * @param $endpoint
     * @param $method
     * @param $user
     *
     * @return false|mixed
     */
    public static function apiConnect($endpoint, $method, $user)
    {
        // get api auth
        $getAuth = HumHubAuth::getJwTAuth();

        // get api
        if ($getAuth->code == 200 ) {
            $token = $getAuth->auth_token;
            $ch = curl_init('https://'. ExtConfigurationUtility::getConfig()['baseurl'] .'/api/'. ExtConfigurationUtility::getConfig()['api'] .'/'.$endpoint);
            $authorization = "Authorization: Bearer ".$token;
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if ($method == 'POST') {
                curl_setopt($ch, CURLOPT_POST, 1 );
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($user));
            }

            if ($method == 'PUT') {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($user));
            }

            if ($method == 'DELETE') {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }

            $result = curl_exec($ch);
            curl_close($ch);

            return json_decode($result);
        } else {
            return false;
        }
    }
}
