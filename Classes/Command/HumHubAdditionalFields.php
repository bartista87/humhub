<?php
namespace SBublies\Humhub\Command;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/

use TYPO3\CMS\Scheduler\AbstractAdditionalFieldProvider;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;

class HumHubAdditionalFields extends AbstractAdditionalFieldProvider {

    public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject) {
        // Initialize selected fields
        if (!isset($taskInfo['feStorageId'])) {
            $taskInfo['feStorageId'] = '1';
            if ((string) $parentObject->getCurrentAction() == 'edit') {
                $taskInfo['feStorageId'] = $task->feStorageId;
            }
        }
        if (!isset($taskInfo['humhubmail'])) {
            $taskInfo['humhubmail'] = '';
            if ((string) $parentObject->getCurrentAction() == 'edit') {
                $taskInfo['humhubmail'] = $task->humhubmail;
            }
        }
        if (!isset($taskInfo['syncfielduid'])) {
            $taskInfo['syncfielduid'] = '12345';
            if ((string) $parentObject->getCurrentAction() == 'edit') {
                $taskInfo['syncfielduid'] = $task->syncfielduid;
            }
        }
        if (!isset($taskInfo['syncbyusername'])) {
            $taskInfo['syncbyusername'] = '1';
            if ((string) $parentObject->getCurrentAction() == 'edit') {
                $taskInfo['syncbyusername'] = $task->syncbyusername;
            }
        }
        if (!isset($taskInfo['syncbyusermail'])) {
            $taskInfo['syncbyusermail'] = '0';
            if ((string) $parentObject->getCurrentAction() == 'edit') {
                $taskInfo['syncbyusermail'] = $task->syncbyusermail;
            }
        }
        $fieldName = 'tx_scheduler[feStorageId]';
        $fieldId = 'feStorageId';
        $fieldValue = $taskInfo['feStorageId'];
        $fieldHtml = '<input id="' . $fieldId . '" name="' . $fieldName . '" type="text" class="form-control form-control-clearable t3js-clearable" value="' . htmlspecialchars($fieldValue) . '" />';
        $additionalFields[$fieldId] = array(
            'code' => $fieldHtml,
            'label' => 'LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:label.field1',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId
        );

        $fieldName2 = 'tx_scheduler[humhubmail]';
        $fieldId2 = 'humhubmail';
        $fieldValue2 = $taskInfo['humhubmail'];
        $fieldHtml2 = '<input id="' . $fieldId2 . '" name="' . $fieldName2 . '" type="text" class="form-control form-control-clearable t3js-clearable" value="' . htmlspecialchars($fieldValue2) . '" />';
        $additionalFields[$fieldId2] = array(
            'code' => $fieldHtml2,
            'label' => 'LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:label.field2',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId2
        );

        $fieldName3 = 'tx_scheduler[syncfielduid]';
        $fieldId3 = 'syncfielduid';
        $fieldValue3 = $taskInfo['syncfielduid'];
        $fieldHtml3 = '<input id="' . $fieldId3 . '" name="' . $fieldName3 . '" type="text" class="form-control form-control-clearable t3js-clearable" value="' . htmlspecialchars($fieldValue3) . '" />';
        $additionalFields[$fieldId3] = array(
            'code' => $fieldHtml3,
            'label' => 'LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:label.field3',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId3
        );

        return $additionalFields;
    }

    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $schedulerModule) {
        $string = $submittedData['feStorageId'];
        if(trim($string) == ''){
            $this->addMessage($GLOBALS['LANG']->sL('LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:msg.missingValue'), \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
                return FALSE;
        }
        $fields = $submittedData['syncfielduid'];
        if(trim($fields) == ''){
            $this->addMessage($GLOBALS['LANG']->sL('LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:msg.missingValue2'), \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
            return FALSE;
        }

        return true;
    }

    public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task) {
        $task->feStorageId = $submittedData['feStorageId'];
        $task->humhubmail = $submittedData['humhubmail'];
        $task->syncfielduid = $submittedData['syncfielduid'];
    }

}
