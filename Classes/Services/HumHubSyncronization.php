<?php
namespace SBublies\Humhub\Services;

/***
 * This file is part of the "humhub" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2023 Stefan Bublies <project@sbublies.de>
 ***/


use TYPO3\CMS\Core\Utility\GeneralUtility;
use SBublies\Humhub\Utility\HumHubApiUtility;
use SBublies\Humhub\Utility\FeUserUtility;
use SBublies\Humhub\Domain\Repository\MappingRepository as MappingRepository;
use SBublies\Humhub\Utility\ExtConfigurationUtility;

class HumHubSyncronization
{

    /**
     * @param $feUserStorage
     * @param $syncId
     * @param $syncUsername
     * @param $syncUsermail
     *  (c) 2023 Stefan Bublies
     */

    public function syncData($feUserStorage, $syncId)
    {

        // add or update user
        foreach(FeUserUtility::readFeUserData($feUserStorage) as $feuser) {

            // get the humhubuser
            $humhubUser = HumHubApiUtility::apiConnect('user/get-by-authclient?name=jwt&id=t3-'.$feuser['uid'], 'GET', array());
            // sort the data array for humhub
            $userData =$this->assignUserData($feuser,$syncId);

            if (isset($humhubUser->code) &&  $humhubUser->code == 404) {
               $response = HumHubApiUtility::apiConnect('user', 'POST', $userData);
                if (isset($response->code) && $response->code == 400) {
                    $errorResponse = '';
                    foreach ((array)$response as $value) {
                        foreach ((array)$value as $value2) {
                            foreach ((array)$value2 as $item) {
                                $errorResponse.= $item;
                                $errorResponse.= PHP_EOL;
                            }
                        }
                    }
                    FeUserUtility::updateFeUser($feuser['uid'], $response->code, $errorResponse);
                } else {
                    FeUserUtility::updateFeUser($feuser['uid'], 200, '');
                }
            } else {
                if ($feuser['lastsynctohumhub'] <= strtotime(ExtConfigurationUtility::getConfig()['syncPeriod'].' hours')) {
                    HumHubApiUtility::apiConnect('user/'.$humhubUser->id, 'PUT', $userData);
                    FeUserUtility::updateFeUser($feuser['uid'], 200, '');
                } else {
                    continue;
                }

                if ($feuser['disable'] == 0) {
                    HumHubApiUtility::apiConnect('user/' . $humhubUser->id, 'PUT', array("account" => array("visibility" => 0, "status" => 1)));
                }
            }
        }

        // soft delete user
        foreach (FeUserUtility::readAllFeUserData($feUserStorage) as $feuser) {
            $humhubUser = HumHubApiUtility::apiConnect('user/get-by-authclient?name=jwt&id=t3-'.$feuser['uid'], 'GET', array());
            if (isset($humhubUser->code) &&  $humhubUser->code == 404) {
                continue;
            } else {
                if ($feuser['username'] == $humhubUser->account->username) {
                    if ($feuser['disable'] == 1) {
                        HumHubApiUtility::apiConnect('user/' . $humhubUser->id, 'PUT',
                            array("account" => array("visibility" => 1, "status" => 0)));
                    }
                    if ($feuser['deleted'] == 1) {
                        HumHubApiUtility::apiConnect('user/' . $humhubUser->id, 'DELETE', array());
                    }
                }
            }
        }
    }


    /**
     * @param $feuser
     * @param $syncId
     * @return array
     */
    protected function assignUserData($feuser, $syncId)
    {
        // field mapping
        $this->mappingRepository = GeneralUtility::makeInstance(MappingRepository::class);
        $mappings = $this->mappingRepository->findByPid($syncId);

        $sortedDatas = [];

        // default fields
        $sortedDatas['account']['authclient_id'] = 't3-'.$feuser['uid'];
        $sortedDatas['account']['authclient'] = 'jwt';
        $sortedDatas['account']['username'] = $feuser['username'];
        $sortedDatas['account']['email'] = $feuser['email'];

        // workaround 05/2023 see issue https://github.com/humhub/rest/issues/124
        $sortedDatas['password']['newPassword'] = md5(time() . rand());

        // additional fields
        foreach($mappings as $mapping) {
            if ($mapping['humhubarray'] == 'account') {
                $sortedDatas['account'][$mapping['humhubfield']] = $feuser[$mapping['t3field']];
            }
            if ($mapping['humhubarray'] == 'profile') {
                $sortedDatas['profile'][$mapping['humhubfield']] = $feuser[$mapping['t3field']];
            }
        }

        return $sortedDatas;
    }
}
