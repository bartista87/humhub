<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'HumHub Integration',
    'description' => 'Synchronization fe users from TYPO3 CMS into HumHub and sso to humhub',
    'category' => 'services',
    'author' => 'Stefan Bublies',
    'author_email' => 'project@sbublies.de',
    'state' => 'beta',
    'clearCacheOnLoad' => 0,
    'version' => '12.4.7',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
