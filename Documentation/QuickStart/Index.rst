
.. include:: /Includes.rst.txt

.. _quick-start:

===========
Quick start
===========

We assume you have composer-based TYPO3 installation and and uses humhub.

.. warning::
	Before you synchronize your fe users for the first time, please **complete the installation and configuration in TYPO3 and humhub**. Otherwise data inconsistency may occur.

.. rst-class:: bignums

#. Install EXT:humhub

   .. code-block:: bash

      composer req sbublies/humhub

   See the chapter on :ref:`installation`. Make sure the TypoScript template is
   included.

#. Add scheduler task to sync the fe user

   Go to the module :guilabel:`System > Scheduler` provided by
   this extension. See also :ref:`add-scheduler-task`.

#. Complete the required EXT settings

   Using the module :guilabel:`Admin tools > Settings` to set the settings. See also :ref:`ext-settings`.

#. Add sso fe plugins

   HumHub in connection with TYPO3 CMS works exclusively with an Auth method via TYPO3. Therefore you have to create one of the two PlugIns to be able to log in to Humhub See chapter :ref:`fe-plugins`.

#. Add field mapping records

   There are a number of fixed fields that are transferred to humhub. However, humhub offers a number of fields and options, so here you have the possibility to link your own fields. See chapter :ref:`mapping-records`.

#. Optional: Delete the base css for the button and forms

   If you don't want to use the CSS you brought, you can delete it via your TypoScript setup. To do this, simply include the following line in the TS setup:

.. code-block:: php
   plugin.tx_humhub._CSS_DEFAULT_STYLE >


#. What needs to be done in humhub

   In Humhub, a number of settings must be affected so that the synchronization works properly and the single sign-on can also be used. See chapter :ref:`humhub-settings`.
