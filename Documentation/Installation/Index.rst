
.. include:: /Includes.rst.txt
.. _installation:

============
Installation
============

It is not recommended to use this extension on productive systems. If you do
make sure you have a current database backup and no editors working on your
page.

After installation you need to include the TypoScript template in your root
pages template.

TYPO3 11 & 12, composer-based
========================

In a current :ref:`composer-based TYPO3 installation <t3start:install>` you can
install this extension like this:

.. code-block:: bash

      composer req sbublies/humhub

The extension will be automatically activated.

.. _installation-legacy:

Legacy TYPO3 installations without composer
===========================================

`Download humhub from the TER (TYPO3 Extension Repository) <https://extensions.typo3.org/extension/humhub>`__

Upload it in :guilabel:`Admin Tools > Extension Manager` and activate it there.

Next steps
==========

Have a look at the :ref:`quick-start`.
