
.. include:: /Includes.rst.txt

.. _known-problems:

==============
Known problems
==============

If you find further limitations, please
`report an issue on GitLab <https://gitlab.com/bartista87/humhub/-/issues>`__.
