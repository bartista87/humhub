
.. include:: /Includes.rst.txt


.. _usage:

=====
Usage
=====

.. note::
   Currently, the extension can only be used with the HumHub Professional Editon. For single sign-on the JWT-Auth module is required. Soon HumHub will release its own TYPO3 Auth module for free. Then this extension can also be used with the HumHub Community Edition.

.. _add-scheduler-task:

Add a scheduler task for syncronization
===================================================

Create a scheduler task in your TYPO3 CMS of the type "HumHub - fe user syncronization" as usual. There you have the following options: :guilabel:`&Folder ID` where the fe users are stored, Optional: :guilabel:`&Mail address` for the email confirmation as well as the :guilabel:`&Folder ID` for the field mapping records.

.. image:: /Images/scheduler_options.png
  :width: 200
  :alt: Scheduler Task

.. _ext-settings:

Set extension settings
===================================================
The extension settings are essential for smooth functionality. Please fill in the fields correctly and carefully. The following options are available:

`baseurl`
		The baseurl from your humhub installation
`username`
		Username from your auth user
`password`
		Password from your auth user
`sharedKey`
		The individual key you generate to configure the JWT token generation for the SSO functionality. Please make sure to use a 256bit strong key
`keyValid`
		Validity of the JWT token

`syncPeriod`
		Value in hours, how often the fe user data should be synchronized

`prettyUrls`
		Enable this option if your humhub installation has pretty urls enabled

`api`
		The version from the humhub api

.. image:: /Images/ext_settings.png
  :width: 200
  :alt: Extension Settings

.. _fe-plugins:

Add fe plugins
===================================================

There are two different frontend plugins. One is an SSO function via link (for example in an active internal area) and the other is an SSO function via form. **Important for the plugin: SSO Login with form is the setting of the fe user folder in the plugin.**

.. image:: /Images/fe_plugins.png
  :width: 200
  :alt: FE PlugIn

.. _mapping-records:

Add field mapping records
===================================================

HumHub is a powerful tool. Consequently, HumHub in the basic version can also provide a range of profile information that does not exist in TYPO3. Furthermore, you can also create your own fields in HumHub. Therefore the extension brings its own field mapping. Here you can synchronize your own fields with the help of the HumHub Rest-API docu: https://marketplace.humhub.com/module/rest/docs/html/user.html#tag/User.

**To use the field mapping, create a system folder in the page tree and create the records there accordingly.**

The following fields are stored in the extension by default and cannot be changed:

`username`
`email`
`authclient`
`authclient_id`

**Important note: You are not allowed to synchronize the password field. It is not needed in the Auth method and the hash from TYPO3 cannot be used in HumHub!**

.. _humhub-settings:

HumHub Settings
===================================================
To set up synchronization, you need to set the following in HumHub:

#.  Go to administration > moduls
#.  Search an install RESTful API modul
#.  Configure the RESTful API modul with following settings:

	#. General > Aktivate "Allow JWT Authentication" and set the auth user

Manual from RESTful API: https://marketplace.humhub.com/module/rest/description

To use the SSO function, you must install the JWT SSO module and configure it accordingly. The documentation for this can be found here: https://marketplace.humhub.com/module/jwt-sso/description

Good to know
===================================================

*   You must add your Humhub admin in typo3 to login with the SSO-API
*   Soon a free TYPO3-Auth module will be available on the HumHub Marketplace

Troubleshooting
===================================================

**What do I do if a fe user does not sync to Humhub?**

The extension stores the synchronization status in the respective fe user. Just as the synchronization time and if the user could not be synchronized also accordingly the error information from HumHub.

You can find the information in the record of the respective fe user > extended

.. image:: /Images/humhub_state.png
  :width: 200
  :alt: HumHub State

**What do I do if TYPO3 does not find the Firebase/JWT/JWT class?**

If you have installed TYPO3 in classic mode, you have to make sure that the php package firebase/php-jwt is available in your system. TYPO3 12 comes with the package directly, in version 11 you have to install it separately. If you have installed TYPO3 in the composer mode, there is a dependency and the firebase package is installed with it and you don't have to do anything else except clearing the cache.
