
.. include:: /Includes.rst.txt
.. _about:

=============================================================
About
=============================================================

:Version:
   {release}

:Language:
   en

:Authors:
   Stefan Bublies and all contributors

:Email:
   project@sbublies.de

:License:
   This extension documentation is published under the
   `Creative Commons Attribution 4.0 International (CC BY 4.0) <https://creativecommons.org/licenses/by/4.0/>`__
   license

TYPO3 and others
=================

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .

Community documentation
=======================

This documentation is community documentation for the TYPO3 extension
humhub

It is maintained as part of this third party extension.

If you find an error or something is missing, please:
`Report a Problem <https://gitlab.com/bartista87/humhub/-/issues>`__

You are welcome to help improve this guide. As this project is hosted on GitLab
there is no "Edit on Github" button available.

Attribution
===========

Thanks to Stefan Bublies, who created this extension and some initial
documentation.
