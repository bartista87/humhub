
.. include:: /Includes.rst.txt

.. _introduction:

============
Introduction
============

.. _what-it-does:

What does it do?
================

This extension can be syncronize fe user from typo3 to humhub and makes a sso login to humhub.

 .. warning::
      Before you synchronize your fe users for the first time, please **complete the installation and configuration in TYPO3 and humhub**. Otherwise data inconsistency may occur.

.. figure:: /Images/scheduler_task.png
   :class: with-shadow
   :alt: The Scheduler Task

   The scheduler task

See the :ref:`Quick start <quick-start>` to get started in the TYPO3 CMS Backend.
