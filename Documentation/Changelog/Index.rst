
.. include:: /Includes.rst.txt

.. _changelog:

==========
Change log
==========

Version 12.4.7
--------------

- new version
- bugfix: change authclient-ID to "jwt"

Version 12.4.6
--------------

- new version
- bugfix sync-auth and add fe-user-info on state create user

Version 12.4.5
--------------

- new version
- add firebase/php-jwt to required in composer.json
- extend documentation

Version 12.4.4
--------------

- new version
- final version of the documentation

Version 12.4.3
--------------

- new version
- first version of the documentation

Version 12.4.2
--------------

- new version
- save error messages from humhub and save synctime, sync period in ext settings (value in hours), small bugfixes

Version 12.4.1
--------------

- new version
- al lot of changes

Version 12.4.0
--------------

- initial extension
