
.. include:: /Includes.rst.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

===============
humhub
===============

This extension can be syncronize fe user from typo3 to humhub and makes a sso login to humhub


.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Quick start <quick-start>`

         .. container:: card-body

            A quick introduction in how to use this extension.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Introduction <introduction>`

         .. container:: card-body

            What does this extension do? See some screenshots

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Usage <usage>`

         .. container:: card-body

            Learn how to use this extension in detail.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Known problems <known-problems>`

         .. container:: card-body

            What are the limitations of this extension? Where can you report
            problems?

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`About <about>`

         .. container:: card-body

            About this extension, the author, the project etc

.. toctree::
   :hidden:
   :titlesonly:

   QuickStart/Index
   Introduction/Index
   Installation/Index
   CommandReference/Index
   Usage/Index
   KnownProblems/Index
   Changelog/Index
   About
   Sitemap
