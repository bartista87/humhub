CREATE TABLE tx_humhub_domain_model_mapping (
    t3field varchar(255) NOT NULL DEFAULT '',
    humhubarray varchar(255) NOT NULL DEFAULT '',
    humhubfield varchar(255) NOT NULL DEFAULT '',
);

CREATE TABLE fe_users (
    lastsynctohumhub int(11) unsigned DEFAULT '0',
    humhubsyncstate varchar(10) DEFAULT '' NOT NULL,
    humhuberror text,
);
