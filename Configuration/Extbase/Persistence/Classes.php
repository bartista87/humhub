<?php
declare(strict_types = 1);

return [
    \SBublies\Humhub\Domain\Model\Dto\User::class => [
        'tableName' => 'fe_users',
    ],
];
