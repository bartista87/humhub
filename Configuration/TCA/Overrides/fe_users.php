<?php
defined('TYPO3') || die();

$tmp_columns = [
    'lastsynctohumhub' => [
        'exclude' => true,
        'label' => 'LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:label.lastsynctohumhub',
        'config' => [
            'type' => 'input',
            'renderType' => 'inputDateTime',
            'eval' => 'datetime,int',
            'default' => 0,
            'range' => [
                'upper' => mktime(0, 0, 0, 1, 1, 2038)
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'readOnly' => 1
        ]
    ],
    'humhubsyncstate' => [
        'exclude' => true,
        'label' => 'LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:label.humhubsyncstate',
        'config' => [
            'type' => 'input',
            'eval' => 'trim',
            'readOnly' => 1
        ]
    ],
    'humhuberror' => [
        'exclude' => true,
        'label' => 'LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:label.humhuberror',
        'config' => [
            'type' => 'text',
            'readOnly' => 1
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tmp_columns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    '--palette--;LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:label.feuserhumhub;humhub',
    '',
    'after:tx_extbase_type'
);

// Add the new palette:
$GLOBALS['TCA']['fe_users']['palettes']['humhub'] = array(
    'showitem' => 'lastsynctohumhub,humhubsyncstate,humhuberror'
);
