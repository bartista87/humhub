<?php
defined('TYPO3') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Humhub',
    'Humhublogin',
    'HumHub – SSO'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Humhub',
    'Humhubloginform',
    'HumHub – SSO'
);
