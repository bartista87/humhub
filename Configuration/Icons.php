<?php

return [
    'humhub-plugin-login' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:humhub/Resources/Public/Icons/Mapping_HumHub.svg'
    ],
];
