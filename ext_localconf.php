<?php
defined('TYPO3') || die('Access denied.');

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Humhub',
        'Humhublogin',
        [
            \SBublies\Humhub\Controller\HumHubLoginController::class => 'index'
        ],
        // non-cacheable actions
        [
            \SBublies\Humhub\Controller\HumHubLoginController::class => 'index'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Humhub',
        'Humhubloginform',
        [
            \SBublies\Humhub\Controller\HumHubLoginController::class => 'formview'
        ],
        // non-cacheable actions
        [
            \SBublies\Humhub\Controller\HumHubLoginController::class => 'formview'
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    humhublogin {
                        iconIdentifier = humhub-plugin-login
                        title = LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:tx_humhub_login.name
                        description = LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:tx_humhub_login.description
                        tt_content_defValues {
                            CType = list
                            list_type = humhub_humhublogin
                        }
                    },
                    humhubloginform {
                        iconIdentifier = humhub-plugin-login
                        title = LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:tx_humhub_loginform.name
                        description = LLL:EXT:humhub/Resources/Private/Language/locallang_db.xlf:tx_humhub_loginform.description
                        tt_content_defValues {
                            CType = list
                            list_type = humhub_humhubloginform
                        }
                    }
                }
                show = *
            }
       }'
    );


    // Scheduler Tasks
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['SBublies\Humhub\Tasks\SyncronizationTask'] = [
        'extension' => 'humhub',
        'title' => 'LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:task.1.label',
        'description' => 'LLL:EXT:humhub/Resources/Private/Language/locallang.xlf:task.1.description',
        'additionalFields' => 'SBublies\Humhub\Command\HumHubAdditionalFields'
    ];

})();
